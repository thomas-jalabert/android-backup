#!/bin/bash
# androidBackup.sh
# For Linux

########################### License ##########################################
# Android Backup
# Copyright (C) 2018  Thomas Jalabert
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
##############################################################################


########################### Manual ###########################################
# This shellScript saves :
  # - contacts (*.vcf at the root of the phone).
  # - records (taken with recorder)
  # - photos and videos taken with the Camera.

# It can be automatically triggered when the device is connected with udev rules (see how to do below, if you are interesting).

# !Caution! : For contacts copy, you have to manually and regularly export vcf files from the phone (go to the contacts --> manage contacts --> import/export contacts --> export in internal storage).

# Launch with root.

# Logs of the executions are written in /var/log/androidBackup/

# Prerequisites :
#  - On the BackupMachine, install "adb" and "android-tools-adb" packages (present in the main debian repo).
#  - On the BackupMachine, install the "adb-sync" command from the Github of Google (https://github.com/google/adb-sync) like this : git clone https://github.com/google/adb-sync ; cd adb-sync ; cp adb-sync /usr/local/bin/
#  - On the phone, enable the developer mode and usb debug (settings, about phone, click seven times on the build version, developer options, usb debug).
#  - In the script, Adapt the variables $idDevice (you can see it with the "adb devices" command after connecting your device) and $user (user which must have access to the backups).
#  - If necessary, adapt $androidPhotosPath and $androidContactsPath (depends on the path of the photos and contacts on the phone).
##############################################################################


############# Trigger the script automaticaly when device is plugged #########

# 1- Plug your phone by usb.

# 2- Pick up usb details (bus and device numbers) with the lsusb command :
# lsusb
# --> Bus 003 Device 012: ID 04e8:6860 Samsung Electronics Co., Ltd Galaxy (MTP).

# 3- Pick up devices details (replace the numbers by yours (003 and 012 in the exemple)) :
# udevadm info -a -p $(udevadm info -q path -n /dev/bus/usb/003/012)
# -->
#  looking at device '/devices/pci0000:00/0000:00:14.0/usb3/3-3':
#    KERNEL=="3-3"
#    SUBSYSTEM=="usb"
#    ATTR{idProduct}=="6860"
#    ATTR{idVendor}=="04e8"
#    ATTR{serial}=="5200c82beeb984c7"

# 4- Say to systemd to follow events for this device --> Edit the file /etc/udev/rules.d/usbTrigger.rules and add this line, adapting with yours values :
# ACTION=="add", SUBSYSTEM=="usb", ATTR{idProduct}=="6860", ATTR{idVendor}=="04e8", ATTR{serial}=="5200c82beeb984c7", OWNER="root", GROUP="root", MODE="0770", TAG+="systemd", ENV{SYSTEMD_ALIAS}+="/sys/subsystem/usb/android"
# Reload udev conf : udevadm control -R

# 5- Create the service unit to be started when device is plugged --> Edit /etc/systemd/system/android-backup.service and enable it :
# [Unit]
# Description=android-backup

# [Service]
# Type=simple
# ExecStart=/root/scripts/androidBackup.sh

# [Install]
# WantedBy=sys-subsystem-usb-android.device
##############################################################################

set -u
echo "Waiting for adb devices"
sleep 5

# Variables.
idDevice="YOUR DEVICE ID" #Your device's ID, as shown in "adb devices"
user="YOUR USER NAME" # The sript will chown the backup for this user (cause launched by root.)
androidPhotosPath="/storage/self/primary/DCIM/Camera/"
androidRecordsPath="/storage/self/primary/Record/"
androidContactsPath="/storage/self/primary/*.vcf"
backupDir="/home/$user/androidBackup"
devices=$(adb devices)
logDirectory="/var/log/androidBackup"
logFile=$logDirectory/$(date +"%F_%H-%M-%S").log
lockFile=/run/lock/androidBackup

# Creating log directory, if doesn't exists.
[ ! -d $logDirectory ] && { mkdir -p $logDirectory || exit ; }

# Comment
echo "Backup in progress, all the logs are in $logFile"

# Attribute the same outputs for all the commands bellow.
{

# Stop the script if lockFile is already present
echo "Verifying if lockFile already exists..."
if [ -f $lockFile ]
then
  echo "script already in execution or last execution badly aborted, remove the lock $lockFile before re-executing the script"
  exit
fi

# Lock creation.
echo "Creating lockFile $lockFile..."
touch $lockFile

# Is the device connected ?
echo "DeviceID to backup = $idDevice"
if [[ $devices != *"$idDevice"* ]]
then
  echo "$idDevice is not connected, backup cancelled"
  rm $lockFile -f
  exit
else
  echo "$idDevice is connected"
fi

# Creating backups directories if don't exist.
[ ! -d $backupDir/contacts ] && { echo "Creating $backupDir/contacts..." ; mkdir -p $backupDir/contacts || { rm $lockFile -f ;  exit ; } ; }
[ ! -d $backupDir/photos ] && { echo "Creating $backupDir/photos..." ; mkdir -p $backupDir/photos || { rm $lockFile -f ;  exit ; } ; }
[ ! -d $backupDir/records ] && { echo "Creating $backupDir/records..." ; mkdir -p $backupDir/records || { rm $lockFile -f ;  exit ; } ; }

# Copying contacts.
echo "Syncing $androidContactsPath in $backupDir/contacts/..."
adb-sync -s $idDevice --reverse $androidContactsPath $backupDir/contacts/ || { rm $lockFile -f ; exit ; }
echo "Finish"

# Copying Records
echo "Syncing $androidRecordsPath in $backupDir/records/..."
adb-sync -s $idDevice --reverse $androidRecordsPath $backupDir/records/ || { rm $lockFile -f ; exit ; }
echo "Finish"

# Copying Photos.
echo "Syncing $androidPhotosPath in $backupDir/photos/..."
adb-sync -s $idDevice --reverse $androidPhotosPath $backupDir/photos/ || { rm $lockFile -f ; exit ; }
echo "Finish"

# Permissions.
echo "Permissions for user $user..."
chown -R $user $backupDir || { rm $lockFile -f ; exit ; }

# If the script came to here --> Backup is OK
echo "backup OK"

#LockFile suppression.
rm $lockFile -f
echo "lockFile deleted"

# Output redirection
} >>$logFile 2>&1
